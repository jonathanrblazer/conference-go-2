import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval_msg(ch, method, properties, body):
    print("  Received approval")
    body_dict = json.loads(body)

    return send_mail(
        "Your presentation has been accepted",
        f"{body_dict['presenter_name']}, We are happy to tell you your presentation, {body_dict['title']}, has been accepted.",
        "admin@conference.go",
        [f"{body_dict['presenter_email']}"],
        fail_silently=False,
    )


def process_rejection_msg(ch, method, properties, body):
    print("  Received rejection")
    body_dict = json.loads(body)

    return send_mail(
        "Your presentation has been rejected",
        f"{body_dict['presenter_name']}, We are sad to tell you your presentation, {body_dict['title']}, has been rejected.",
        "admin@conference.go",
        [f"{body_dict['presenter_email']}"],
        fail_silently=False,
    )


def process_approval():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval_msg,
        auto_ack=True,
    )
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection_msg,
        auto_ack=True,
    )
    channel.start_consuming()  # NEVER ENDSSSS!!!!
    return


while True:
    try:
        process_approval()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)


# def process_rejection():
#     parameters = pika.ConnectionParameters(host="rabbitmq")
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue="presentation_rejections")
#     channel.basic_consume(
#         queue="presentation_rejections",
#         on_message_callback=process_rejection_msg,
#         auto_ack=True,
#     )
#     channel.start_consuming()
#     return
