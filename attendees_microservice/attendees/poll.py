import json
import requests

from .models import ConferenceVO, Attendee


def get_conferences():
    print("GETTING CONFERENCES!")
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )

    # print("GETTING ATTENDEES!")
    # num_conferences = ConferenceVO.objects.all().count()
    # for conf_num in range(1, num_conferences + 1):
    #     url = f"http://monolith:8000/api/conferences/{conf_num}/attendees/"
    #     response = requests.get(url)
    #     content = json.loads(response.content)
    #     for attendee in content["attendees"]:
    #         Attendee.objects.update_or_create(**content)


## NO THIS STUPID CODE DIDN'T WORK DUMMY!!
