from django.apps import AppConfig
from os import getcwd


class AttendeesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "attendees"


print("Hello Jonathan, you are running Attendees in ", getcwd())
