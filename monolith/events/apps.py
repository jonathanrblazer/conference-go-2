from django.apps import AppConfig
from os import getcwd


class EventsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "events"


print("Hello Jonathan! You are running Monolith in ", getcwd())
