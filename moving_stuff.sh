mkdir monolith
mv accounts monolith
cp -r common monolith
mv conference_go monolith
mv data monolith
mv events monolith
mv presentations monolith
mv Dockerfile monolith
cp Dockerfile.dev monolith
mv manage.py monolith
cp requirements.txt monolith
